import { resolve } from 'path';
import { config } from 'dotenv-safe';

class EnvConfig {
  constructor() {
    config({ path: resolve(__dirname, '../../.env') });
  }
}

export default new EnvConfig();
