import express, { Application } from 'express';
import cors from 'cors';
import chatRouter from '@route/chat.route';

class ExpressConfig {
  constructor(app: Application) {
    this.middlewares(app);
    this.routes(app);
  }

  middlewares(app: Application): void {
    app.use(express.urlencoded({ extended: false }));
    app.use(express.json());
    app.use(cors());
  }

  routes(app: Application): void {
    app.use('/', chatRouter);
  }
}

export default (app: Application) => new ExpressConfig(app);
