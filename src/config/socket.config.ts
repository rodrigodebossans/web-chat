import { Server as SocketServer, Socket } from 'socket.io';
import { Server as HttpServer } from 'http';
import chatService from '@service/chat.service';

class SocketConfig {
  private io: SocketServer;

  constructor(server: HttpServer) {
    this.io = new SocketServer(server);
    chatService(this.io);
  }
}

export default (server: HttpServer) => new SocketConfig(server);
