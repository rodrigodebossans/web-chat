import { ClientMessage, ServerMessage } from 'src/interface/chat.interface';
import { Socket, Server as SocketServer } from 'socket.io';
import { SocketEvents } from '@enum/socket-events.enum';

class ChatService {
  private io: SocketServer;

  constructor(io: SocketServer) {
    this.io = io;
    this.io.on(SocketEvents.onConnect, (socket: Socket) =>
      this.onConnection(socket)
    );
  }

  onConnection(socket: Socket): void {
    socket.on(SocketEvents.clientMessage, (message: ClientMessage) =>
      this.onClientMessage(message)
    );
    socket.on(SocketEvents.onDisconnect, () =>
      console.log('user disconnected')
    );
  }

  onClientMessage(message: ClientMessage): void {
    const serverMessage: ServerMessage = { ...message, now: Date.now() };
    this.io.emit(SocketEvents.serverMessage, serverMessage);
  }
}

export default (io: SocketServer) => new ChatService(io);
