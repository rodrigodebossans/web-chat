import { Request, Response, Router } from 'express';
import { resolve } from 'path';

class ChatRoute {
  router: Router;

  constructor() {
    this.router = Router();
    this.router
      .route('/')
      .get((req: Request, res: Response) =>
        res.sendFile(resolve(__dirname, '../resource/index.html'))
      );
  }
}

export default new ChatRoute().router;
