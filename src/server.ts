import '@config/env.config';
import express, { Application } from 'express';
import expressConfig from '@config/express.config';
import { createServer, Server as HttpServer } from 'http';
import socketConfig from '@config/socket.config';

class Server {
  private app: Application;
  private port: number;
  private server: HttpServer;

  constructor() {
    this.app = express();
    this.port = parseInt(process.env.PORT ?? '') | 3000;
    this.server = createServer(this.app);
    this.start();
  }

  start(): void {
    try {
      expressConfig(this.app);
      socketConfig(this.server);

      this.server.listen(this.port, () => {
        console.info(`Listening at http://localhost:${this.port}`);
      });
    } catch (error) {
      console.error({ error });
    }
  }
}

export default new Server();
