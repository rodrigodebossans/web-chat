export enum SocketEvents {
  onConnect = 'connection',
  onDisconnect = 'disconnect',
  clientMessage = 'clientMessage',
  serverMessage = 'serverMessage',
}
