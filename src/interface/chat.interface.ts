interface Message {
  username: string;
  content: string;
}

export interface ClientMessage extends Message {}

export interface ServerMessage extends Message {
  now: number;
}
